/**
 * SECTION - JS Synchronous cs asynchronous
 * javascript is by default synchronous, meaning that only one statement will be executed at a time
 * this can be proven when a statement has an error, Javascript will not proceed with the next statement
 */
// console.log("Hello World!");
// // conosole.log("Hello Again");
// console.log("Hello Again");
// console.log("Goodbye");
// for(let i = 1; i<=1500;i++){
//     console.log(i);
// }
// console.log("It's me again");

// Fetch API - Allows us to asynchronously request for a resource(data)
// a promise is an object that represents the eventual completion of an asynchronous function and its resulting value
/**
 * SYNTAX
 *      fetch(url);
 */

// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
//retrieves all posts following the REST API(retrieve, /posts, GET)
// fetch method will return a promise that resolves to a response object
// .then method captures the response object and returns another "promise" which will eventually be resolved or rejected
// fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(json => console.log(json));

console.log("Hello");

async function fetchData(){
    let result = await fetch("https://jsonplaceholder.typicode.com/posts");
    console.log(result);
    console.log(typeof result);
    console.log(result.body);
    let json = await result.json()
    console.log(json);
};

fetchData();
console.log("hello");

/**
 * SECTION - creating a POST method in js
 */

fetch("https://jsonplaceholder.typicode.com/posts",{
    method: "POST",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        title:"New Post",
        userId: 1
    })
}).then(response => response.json()).then(json => console.log(json));


/**
 * SECTION - creating a PUT method in js
 */

 fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "PUT",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        id:1,
        title: "Updated Post",
        body:"Hello again",
        userId: 1
    })
}).then(response => response.json()).then(json => console.log(json));
/**
 * SECTION - creating a PATCH method in js
 */
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "PATCH",
    headers:{
        "Content-Type":"application/json"
    },
    body:JSON.stringify({
        title: "Corrected Post"
    })
}).then(response => response.json()).then(json => console.log(json));

/**
 * SECTION - creating a DELTE method in js
 */
 fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "DELETE"});

/**
 * SECTION - filter posts
 */
 fetch("https://jsonplaceholder.typicode.com/posts?userId=1").then(response => response.json()).then(json => console.log(json));


 /**
  * Retrieving comments for a specific post/ accessing nested/embedded comments
  */

  fetch("https://jsonplaceholder.typicode.com/posts/1/comments").then(response => response.json()).then(json => console.log(json));